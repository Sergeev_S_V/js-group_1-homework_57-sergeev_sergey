

  const tasks = [
    {id: 234,
      title: 'Create user registration API',
      timeSpent: 4,
      category: 'Backend',
      type: 'task'},
    {id: 235,
      title: 'Create user registration UI',
      timeSpent: 8,
      category: 'Frontend',
      type: 'task'},
    {id: 237,
      title: 'User sign-in via Google UI',
      timeSpent: 3.5,
      category: 'Frontend',
      type: 'task'},
    {id: 238,
      title: 'User sign-in via Google API',
      timeSpent: 5,
      category: 'Backend',
      type: 'task'},
    {id: 241,
      title: 'Fix account linking',
      timeSpent: 5,
      category: 'Backend',
      type: 'bug'},
    {id: 250,
      title: 'Fix wrong time created on new record',
      timeSpent: 1,
      category: 'Backend',
      type: 'bug'},
    {id: 262,
      title: 'Fix sign-in failed messages',
      timeSpent: 2,
      category: 'Frontend',
      type: 'bug'},
  ];

  let spentTimeOnFrontEnd = tasks.reduce((acc, item) => {
    item.category === 'Frontend' ? acc += item.timeSpent : null;
    return acc;
  }, 0);
  console.log(spentTimeOnFrontEnd);



  let spentTimeOnBug = tasks.reduce((acc, item) => {
    item.type === 'bug' ? acc += item.timeSpent : null;
    return acc;
  }, 0);
  console.log(spentTimeOnBug);



  let amountTaskWithWordUI = tasks.reduce((acc, item) => {
    item.title.indexOf('UI') > -1 ? acc++ : null;
    return acc;
  }, 0);
  console.log(amountTaskWithWordUI);



  let amountTasksEveryCategory = tasks.reduce((acc, item) => {
    acc[item.category] ? acc[item.category]++ : acc[item.category] = 1;
    return acc;
  }, {});
  console.log(amountTasksEveryCategory);



  let checkOnSpentTime = tasks
    .filter(task => task.timeSpent > 4)
    .map(task => ({title: task.title, category: task.category}));
  console.log(checkOnSpentTime);


