import React, { Component } from 'react';
import './App.css';
import AddController from "./Components/AddController/AddController";
import CurrentCosts from "./Containers/CurrentCosts/CurrentCosts";

class App extends Component {

  state = {
    currentCosts: [],
    costName: '',
    costPrice: 0,
    category: '',
    totalSpent: 0,
  };

  id = 0;



  changeCostName = event => this.setState({costName: event.target.value});

  changeCostPrice = event => this.setState({costPrice: parseInt(event.target.value)});

  addNewCost = () => {
    let currentCosts = [...this.state.currentCosts];
    let newCost;
    if (this.state.costName.length > 0 && this.state.costPrice > 0) {
      newCost = {
        name: this.state.costName,
        price: this.state.costPrice,
        id: this.id,
        category: this.state.category,
      };
      currentCosts.push(newCost);
      this.state.totalSpent += this.state.costPrice;
      this.id++;
    }
    this.setState({currentCosts});
  };

  removeCost = (id) => {
    let index = this.state.currentCosts.findIndex(cost => cost.id === id);
    let currentCosts = [...this.state.currentCosts];
    this.state.totalSpent -= currentCosts[index].price;
    currentCosts.splice(index, 1);

    this.setState({currentCosts});
  };

  selectedCategory = (event) => {
    this.state.category = event.target.value;
  };


  render() {
    return (
      <div className="App">
        <AddController changeName={this.changeCostName}
                       changePrice={this.changeCostPrice}
                       addNewCost={this.addNewCost}
                       select={this.selectedCategory}
        />
        <CurrentCosts totalSpent={this.state.totalSpent}
                      currentCosts={this.state.currentCosts}
                      category={this.state.category}
                      remove={this.removeCost}
        />
      </div>
    );
  }
}

export default App;
