import React from 'react';
import './CurrentCosts.css';
import CurrentCost from "../../Components/CurrentCost";
import Chart from "../../Components/Chart";

const CurrentCosts = (props) => {
  let percentEachCategory = props.currentCosts.reduce((acc, item) => {
    acc[item.category]
      ? acc[item.category] += (item.price / props.totalSpent) * 100
      : acc[item.category] = (item.price / props.totalSpent) * 100;

    return acc;
  }, {});

  return (
      <div className='CurrentCosts'>
        {props.currentCosts.map(cost => (
            <CurrentCost name={cost.name}
                         price={cost.price}
                         remove={() => props.remove(cost.id)}
                         key={cost.id}
                         category={cost.category}/>
            )
        )}
        <p className='totalSpent'>Total spent: {props.totalSpent} KGS</p>
        <Chart percent={percentEachCategory}/>
      </div>
  );
};

export default CurrentCosts;