import React from 'react';
import './Chart.css';

const Chart = (props) => {
  let percentCategory = props.percent;
  let entertainment = percentCategory.Entertainment
    ? percentCategory.Entertainment
    : 0;
  let car = percentCategory.Car
    ? percentCategory.Car
    : 0;
  let food = percentCategory.Food
    ? percentCategory.Food
    : 0;

  return (
    <div>
      <div className='chart'>
        <div style={{width: `${entertainment}%`}} className='Entertainment'/>
        <div style={{width: `${car}%`}} className='Car'/>
        <div style={{width: `${food}%`}} className='Food'/>
      </div>
      <div className='colorCategory'>
        <span className='orange'/><p>Entertainment</p>
        <span className='blue'/><p>Car</p>
        <span className='green'/><p>Food</p>
      </div>
    </div>

  );
};


export default Chart;