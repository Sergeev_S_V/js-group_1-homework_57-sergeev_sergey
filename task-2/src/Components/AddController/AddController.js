import React from 'react';
import './AddController.css';

const AddController = (props) => {
  return (
    <div className='AddController'>
      <p className='welcome'>Welcome to the app for personal finance</p>
      <input className='ItemName'
             placeholder='Item name'
             type="text"
             onChange={props.changeName}/>
      <div className='EnterCost'>
        <input className='Cost'
               placeholder='Cost'
               type="text"
               onChange={props.changePrice}/>
        <span> KGS</span>
      </div>
        <select onChange={props.select} className='select'>
          <option selected style={{display: 'none'}}>Choose category</option>
          <option>Entertainment</option>
          <option>Car</option>
          <option>Food</option>
        </select>
      <button className='AddBtn' onClick={props.addNewCost}>Add</button>
    </div>
  );
};

export default AddController;