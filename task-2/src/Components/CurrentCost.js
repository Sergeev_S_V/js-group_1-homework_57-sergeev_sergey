import React from 'react';
import './CurrentCost.css';

const CurrentCost = (props) => (
  <div className='CurrentCost'>
    <p className='category'>Category:
      <span style={{fontWeight: 'bold'}}> {props.category}</span>
    </p>
    <button className='remove' onClick={props.remove}>X</button>
    <p className='name'>{props.name}</p>
    <p className='price'>
      <span>{props.price}</span> KGS
    </p>
  </div>
);

export default CurrentCost;